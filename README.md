# Scraper for politicfacts.com

## Step 1 - Database
1. Create new MySQL database and import `posts.sql`
2. change database credentials in `spiders/db.py`

## Step 2 - Requirements
* maybe you need to create virtual env, too.

`pip install -r requirements.txt`


## Step 3 - Run spider
`scrapy crawl pfacts`