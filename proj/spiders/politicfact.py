import scrapy
from proj.spiders.db import *


class QuotesSpider(scrapy.Spider):
    name = "pfacts"
    allowed_domains = ["politifact.com"]
    start_urls = [
            'https://www.politifact.com/article/list/',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.db_inst = MySQLStorePipeline()

    def parse(self, response):
        for article in response.css('.m-teaser'):
            href = response.urljoin(article.css('.m-teaser__title a ::attr(href)').get())
            yield scrapy.Request(
                response.urljoin(href),
                callback=self.parse_detail
            )
            

        # comment lines below to prevent next page crawling
        next_page = response.css('li.m-list__item a')
        if next_page:
            href = next_page[-1].css('::attr(href)').get()
            next_url = response.urljoin(href)
            yield scrapy.Request(
                next_url,
                callback=self.parse
            )
        

    def parse_detail(self, response):
        data = {
            "title": response.css(".m-statement__quote ::text").get(),
            "url": response.request.url,
            "content": ' '.join(response.css("article.m-textblock *::text").getall()),
            "author": response.css(".m-author__content a ::text").get(),
            "date": response.css(".m-author__date ::text").get(),
            "tags": ", ".join(response.css(".c-tag span ::text").getall()),
        }
        try:
            self.db_inst.process_item(data)
        except Exception as err:
            self.logger.info("DATABASE ERROR:" + str(err))
        yield data