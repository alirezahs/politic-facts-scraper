import mysql.connector

class MySQLStorePipeline(object):
    def __init__(self):
        self.conn = mysql.connector.connect(
                host="localhost",
                user="root",
                password="12345678",
                database="ir_scraper"
            )
        self.cursor = self.conn.cursor()

    def process_item(self, item):
        # try:
            self.cursor.execute("""
                INSERT INTO posts(url, title, content, date, author, tags)
                VALUES(%s, %s, %s, %s, %s, %s)
            """, (
                item['url'].encode('utf-8'),
                item['title'].encode('utf-8'),
                item['content'].encode('utf-8'),
                item['date'].encode('utf-8'),
                item['author'].encode('utf-8'),
                item['tags'].encode('utf-8'),
            ))
            self.conn.commit()   
        # except mysql.connector.Error as e:
        #     print("item %d: %s" % (e.args[0], e.args[1]))
            return item